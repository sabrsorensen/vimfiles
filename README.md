Overview
========

This repo is mostly for my personal use, keeping track of my vimrc and vimfiles and allowing me to replicate setups I know and
like on my various systems. I use git submodules to keep track of the various plugins I use, and this readme should be kept fairly
up-to-date with information on settings and plugins.

Usage/Installation
------------------
```
git clone https://github.com/sabrsorensen/my_dotvim.git ~/.vim 
cd ~/.vim 
git submodule update --init 
ln -s ~/.vim/vimrc ~/.vimrc
```

Colorscheme
-----------

I'm currently a fan of [desertEx](http://www.vim.org/scripts/script.php?script_id=1693) with a couple modifications to make it a
little more readable on a transparent background.

Plugins
-------

These plugins are all installed as git submodules, which allows me to easily update the plugins as necessary, and clone the
preferred version of each plugin upon initial checkout.

###Pathogen
[vim-pathogen](https://github.com/tpope/vim-pathogen) is tpope's vim runtimepath manager. Pathogen allows you to take vim plugins
and keep them in their own folder inside .vim/bundle (or .vim/wherever, if you call pathogen#infect(wherever) in your vimrc). This
allows for easy installation and removal of plugins.

###CSApprox
[CSApprox](https://github.com/vim-scripts/CSApprox) is a utility to adapt 256color colorschemes to running in a terminal.
Transparent backgrounds don't work very well, thus the 
```viml
let g:CSApprox_hook_post = ['hi Normal  ctermbg=NONE ctermfg=NONE', 'hi NonText ctermbg=NONE ctermfg=NONE' ]
```
line in my vimrc.

###ScrollColors
[ScrollColors](https://github.com/vim-scripts/ScrollColors) lets you walk through installed colorschemes using the arrow keys.
Easier than sourcing or applying each colorscheme. Not really useful right now since I like desertEx quite a bit, but still nice
to have on hand.

###Syntastic
[Syntastic](https://github.com/vim-scripts/Syntastic) provides stronger syntax checking functionality, and adds some features like
syntax checking on file/buffer open/write/close and error messages. I've found it useful for Perl scripting, since I don't have to
run the script to check that I wrote everything correctly.

###md-vim
[md-vim](https://github.com/hughbien/md-vim) is a plugin for syntax checking and coloring Markdown files. I installed it pretty
much for working on this README, but I'm sure it'll be nifty in the future. 

###NerdTree
[NerdTree](https://github.com/scrooloose/nerdtree) provides a pretty easy to use and simple file explorer, which I've bound to
Ctrl+T to open and close.

###Simple Pairs
[simple-pairs](https://github.com/vim-scripts/simple-pairs) will automatically insert closing pairs for ', ", [, {, and (. Saves
some keystrokes.

###SnipMate
[David Halter's vim-snipmate](https://github.com/davidhalter/vim-snipmate) is a plugin to insert pre-defined code snippets,
like for loops. I haven't used it yet, nor defined any code snippets, but it seems like it'd be really useful once I figure out
what I frequently use. Requires the below dependencies:
*   [tlib_vim](https://github.com/tomtom/tlib_vim.git)
*   [vim-addon-mw-utils](https://github.com/MarcWeber/vim-addon-mw-utils.git)
*   [snipmate-snippets](https://github.com/honza/snipmate-snippets.git)

###Supertab
[Supertab](https://github.com/ervandew/supertab) adds some nice autocompletion features when you use \<Tab\>.

###TagBar
[TagBar](https://github.com/majutsushi/tagbar) provides a window showing function and variable declarations of the currently
active file. Similar to some of the tree browsing features of other IDEs like PyCharm and Eclipse. Requires Exuberant Ctags on
your system.

###buftabs
[buftabs](https://github.com/vim-scripts/buftabs) provides a small toolbar containing tabs for each of your open file buffers. 
I mapped F1 and F2 to move backward and forward, respectively, through my tabs/buffers.
