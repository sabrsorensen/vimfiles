filetype off
set nocompatible
"Force vim to detect a 256color terminal
set t_Co=256

"Source pathogen
runtime bundle/vim-pathogen/autoload/pathogen.vim
"Initialize pathogen
call pathogen#infect()
call pathogen#helptags()

"syntax enable plays nicer with custom colorings
syntax enable
colorscheme desertEx
filetype plugin indent on

"TagBar keymappings
nmap <F8> <ESC>:TagbarToggle<cr>
imap <F8> <ESC>:TagbarToggle<cr>i

"buftabs bindings
noremap <F1> :bprev<CR>
noremap <F2> :bnext<CR>

"Pretty statusbar
set laststatus=2
let g:buftabs_in_statusline=1
"shows up as:
"buffers: [Buf#-Filename] [current buffer filetype] Line:## Col:##"
set statusline=%{strftime('%c',getftime(expand('%')))}%=buffers:\ %{buftabs#statusline()}\ %y\ Line:%l\ Col:%c

"Control T to toggle NerdTree
nnoremap <C-t> :NERDTreeToggle<cr>

"Alt-Arrow keys to navigate around Vim windows
map <silent> <A-Up> <C-W><Up>
map <silent> <A-Down> <C-W><Down>
map <silent> <A-Left> <C-W><Left>
map <silent> <A-Right> <C-W><Right>
imap <silent> <A-Up> <C-W><Up>
imap <silent> <A-Down> <C-W><Down>
imap <silent> <A-Left> <C-W><Left>
imap <silent> <A-Right> <C-W><Right>

"Ctrl+Up/Down/Left/Right to resize split windows
"Moves the line between split windows up (top window shrinks)
map <silent> <C-Up> <C-W>+
imap <silent> <C-Up> <C-W>+i
"Moves the line between split windows down (bottom window shrinks)
map <silent> <C-Down> <C-W>-
imap <silent> <C-Down> <C-W>-i
"Moves the line between split windows left (left window shrinks)
map <silent> <C-Left> <C-W><
imap <silent> <C-Left> <C-W><i
"Moves the line between split windows right (right window shrinks)
map <silent> <C-Right> <C-W>>
imap <silent> <C-Right> <C-W>>i

"Highlighting trailing spaces at the end of the line
highlight default link EndOfLineSpace EOLSpace
match EndOfLineSpace / \+$/
autocmd InsertEnter * hi link EndOfLineSpace Normal
autocmd InsertLeave * hi link EndOfLineSpace EOLSpace

"Turns paste mode on/off for inserting code without screwing with the
"indentation/whitespace
set pastetoggle=<F4>
set equalalways
"set mouse=a
set hidden
set wildmenu
set incsearch
"set nohlsearch
set backspace=indent,eol,start
set smartindent
set autoindent
set smarttab
set nostartofline
set ruler
set title
set confirm
set visualbell
set number
"Y to yank to the end of the line
map Y y$

"Keep my lines clean
set nowrap
"Ignore case during searches
set ic
"Override ignore case if pattern has upper-case characters
set scs

"Use control+N to search through autocomplete
set complete=.,w,b,u,i,t

"Personal tab preferences, 4 spaces to indent, tabs expand to 4 spaces
set shiftwidth=4
set tabstop=4
set expandtab

"Necessary for tranparent background when using CSApprox
let g:CSApprox_hook_post = ['hi Normal  ctermbg=NONE ctermfg=NONE', 'hi NonText ctermbg=NONE ctermfg=NONE' ]

"Cleanup swap files and backups
set backupdir=~/.backups,~/tmp
set directory=~/.swaps,~/tmp
set viewdir=~/.view,~/tmp

"Visual indicator of possible code folds, 2 columns wide
"Would like to replace with dynamic "if folds are detected" function
set foldcolumn=2

"Scrolling when you get close to the edge of the screen, rather than jump *at*
"the edge
set scrolloff=10
set sidescrolloff=15
set sidescroll=1

"Experiments

"Functions

function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction

com! DiffSaved call s:DiffWithSaved()
