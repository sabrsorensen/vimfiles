if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufRead,BufNewFile *.apg              setfiletype apg
  au! BufRead,BufNewFile svn-commit*.tmp    setfiletype svncommit
  au! BufRead,BufNewFile *.md               setfiletype md
  au! FileType make                         setlocal noexpandtab "Makefiles need tabs, not spaces
  au! BufRead,BufNewFile *gitconfig*        setfiletype gitconfig
  au! BufRead,BufNewFile *COMMIT_EDITMSG*   setfiletype gitcommit
  au! BufRead,BufNewFile *conkyrc*          setfiletype conkyrc
  au! BufRead,BufNewFile *.git/*/config*      setfiletype gitconfig
augroup END
